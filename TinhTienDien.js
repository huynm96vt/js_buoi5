function TinhTienDien(){
    var name = document.getElementById("name").value;
    var kW = document.getElementById("kW").value*1;
    var tienphaitra = null;
    if(kW<=50){
        tienphaitra = kW*500;
    }
    else if(kW<=100){
        tienphaitra = 50*500 + (kW-50)*650;
    }
    else if(kW<=200){
        tienphaitra = 50*500 + 50*650 + (kW-100)*850;
    }
    else if(kW<=350){
        tienphaitra = 50*500 + 50*650 + 100*850 +(kW-200)*1100;
    }
    else {
        tienphaitra = 50*500 + 50*650 + 100*850 + 150*1100 +(kW-350)*1300;
    }

    document.getElementById("bill").innerHTML = `${name} <br> Tien dien:  ${tienphaitra}`;
}