function XetTuyen() {
  var mon1 = document.getElementById("mon1").value * 1;
  var mon2 = document.getElementById("mon2").value * 1;
  var mon3 = document.getElementById("mon3").value * 1;
  var diemchuan = document.getElementById("diemchuan").value * 1;
  var doituong = document.getElementById("doituong").value;
  var khuvuc = document.getElementById("khuvuc").value;
  var tongdiem = mon1 + mon2 + mon3;
  switch (khuvuc) {
    case "A":
      tongdiem += 2;
      break;
    case "B":
      tongdiem += 1;
      break;
    case "C":
      tongdiem += 0.5;
      break;
  }
  switch (doituong) {
    case "1":
      tongdiem += 2.5;
      break;
    case "2":
      tongdiem += 1.5;
      break;
    case "3":
      tongdiem += 1;
      break;
  }
  if (tongdiem >= diemchuan && mon1 != 0 && mon2 != 0 && mon3 != 0) {
    document.getElementById(
      "ketqua"
    ).innerHTML = `Dau <br> Tong diem: ${tongdiem}`;
  } else {
    document.getElementById("ketqua").innerText = "Rot";
  }
}
